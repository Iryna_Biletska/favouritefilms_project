import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import play.Application;
import play.GlobalSettings;

public class ApplicationGlobalSettings extends GlobalSettings {


    private AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

    @Override
    public void onStart(Application app) {
        super.onStart(app);

        // AnnotationConfigApplicationContext can only be refreshed once, but we do it here even though this method
        // can be called multiple times. The reason for doing during startup is so that the Play configuration is
        // entirely available to this application context.
        applicationContext.scan("controllers", "services", "models");
        applicationContext.refresh();

        // This will construct the beans and call any construction lifecycle methods e.g. @PostConstruct
        applicationContext.start();
    }

    @Override
    public void onStop(Application app) {
        // This will call any destruction lifecycle methods and then release the beans e.g. @PreDestroy
        applicationContext.close();

        super.onStop(app);
    }

    public <A> A getControllerInstance(Class<A> clazz) throws Exception {
        return applicationContext.getBean(clazz);
    }
}
