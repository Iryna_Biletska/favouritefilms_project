package models;

public interface FilmsDAO {

    boolean isFavourite(long filmId);

    void addFilmToList(long filmId, long listId);
}
