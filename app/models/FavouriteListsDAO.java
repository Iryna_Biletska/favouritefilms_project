package models;

import models.entities.FavouriteFilm;
import models.entities.FavouriteList;

import java.util.List;

public interface FavouriteListsDAO {

    List<FavouriteList> getAllLists();

    List<FavouriteFilm> getListFilms(long listId);

    String getSharingLink(long listId);

    void createList(String listName);

    FavouriteList getListBySharingLink(String partOfSharingLink);
}
