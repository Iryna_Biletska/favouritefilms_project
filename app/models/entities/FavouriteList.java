package models.entities;

import com.avaje.ebean.Model;

import javax.persistence.*;

@Entity
public class FavouriteList extends Model {

    @Id @GeneratedValue
    public long id;

    public String listName;

    public String sharingLink;
}
