package models;

import com.avaje.ebean.Ebean;
import models.entities.FavouriteFilm;
import models.entities.FavouriteList;
import org.springframework.stereotype.Repository;

import javax.inject.Singleton;

@Repository
public class FilmsDAOImpl implements FilmsDAO {

    @Override
    public boolean isFavourite(long filmId) {
        FavouriteFilm favFilm = Ebean.find(FavouriteFilm.class, filmId);
        return favFilm != null;
    }

    @Override
    public void addFilmToList(long filmId, long listId) {
        FavouriteFilm favFilm = new FavouriteFilm();
        favFilm.id = filmId;
        favFilm.listId = listId;
        favFilm.save();
    }
}
