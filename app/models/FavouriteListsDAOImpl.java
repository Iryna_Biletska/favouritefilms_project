package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import models.entities.FavouriteFilm;
import models.entities.FavouriteList;
import org.springframework.stereotype.Repository;

import javax.inject.Singleton;
import java.sql.Timestamp;
import java.util.List;

@Repository
public
class FavouriteListsDAOImpl implements FavouriteListsDAO {

    @Override
    public List<FavouriteList> getAllLists() {
        return Ebean.find(FavouriteList.class).findList();
    }

    @Override
    public List<FavouriteFilm> getListFilms(long listId) {
        return Ebean.find(FavouriteFilm.class).where().eq("list_id", listId).findList();
    }

    @Override
    public String getSharingLink(long listId) {
        return Ebean.find(FavouriteList.class, listId).sharingLink;
    }

    @Override
    public void createList(String listName) {
        String partOfSharingLink = new Timestamp(System.currentTimeMillis()).toString();
        FavouriteList favList = new FavouriteList();
        favList.listName = listName;
        favList.sharingLink = partOfSharingLink;
        favList.save();
    }

    @Override
    public FavouriteList getListBySharingLink(String partOfSharingLink) {
        return Ebean.find(FavouriteList.class).where().eq("sharing_link", partOfSharingLink).findUnique();
    }
}