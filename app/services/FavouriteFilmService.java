package services;

import models.FilmsDAO;
import models.entities.FavouriteFilm;
import models.entities.FavouriteList;

import java.util.List;

public interface FavouriteFilmService {

    void addFilmToFavourite(long filmId, long listId);

    boolean isFavouriteFilm(long filmId);

    void createFavouriteList(String listName);

    String shareFavouriteList(long listId);

    List<FavouriteList> getAllLists();

    List<FavouriteFilm> getListFilms(long listId);

    FavouriteList getList(String partOfSharingLink);
}
