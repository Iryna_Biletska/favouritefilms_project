package services;

import models.FavouriteListsDAO;
import models.FavouriteListsDAOImpl;
import models.FilmsDAO;
import models.FilmsDAOImpl;
import models.entities.FavouriteFilm;
import models.entities.FavouriteList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.inject.Qualifier;
import javax.inject.Singleton;
import java.sql.Timestamp;
import java.util.List;

@Component
public class FavouriteFilmServiceImpl implements FavouriteFilmService {

    @Autowired
    private FavouriteListsDAOImpl favouriteListsDAO;

    @Autowired
    private FilmsDAOImpl filmsDAO;

    @Override
    public void addFilmToFavourite(long filmId, long listId) {
        filmsDAO.addFilmToList(filmId, listId);
    }

    @Override
    public boolean isFavouriteFilm(long filmId) {
        return filmsDAO.isFavourite(filmId);
    }

    @Override
    public void createFavouriteList(String listName) {
        favouriteListsDAO.createList(listName);
    }

    @Override
    public String shareFavouriteList(long listId) {
        return favouriteListsDAO.getSharingLink(listId);
    }

    @Override
    public List<FavouriteList> getAllLists() {
        return favouriteListsDAO.getAllLists();
    }

    @Override
    public List<FavouriteFilm> getListFilms(long listId) {
        return favouriteListsDAO.getListFilms(listId);
    }

    @Override
    public FavouriteList getList(String partOfSharingLink) {
        return favouriteListsDAO.getListBySharingLink(partOfSharingLink);
    }
}
