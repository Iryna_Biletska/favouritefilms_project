package controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import play.mvc.*;

import services.FavouriteFilmService;
import views.html.*;

public class Application extends Controller {

    private ApplicationContext ctx = new ClassPathXmlApplicationContext("components.xml");

    public Result index() {
        return ok(main.render(getFavouriteFilmService().getAllLists()));
    }

    protected FavouriteFilmService getFavouriteFilmService() {
        return (FavouriteFilmService) ctx.getBean("favouriteFilmService");
    }

}
