package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.entities.FavouriteFilm;
import models.entities.FavouriteList;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import play.libs.F;
import play.libs.Json;
import play.libs.ws.WS;
import play.libs.ws.WSResponse;
import play.mvc.Controller;
import play.mvc.Result;
import services.FavouriteFilmService;
import views.html.listCreation;
import views.html.listFilms;
import views.html.main;
import views.html.sharedList;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FilmController extends Controller {

    private ApplicationContext ctx = new ClassPathXmlApplicationContext("components.xml");

    public Result addFilmToFavouriteList() {
        JsonNode json = request().body().asJson();
        long filmId = json.findPath("filmId").asLong();
        long listId = json.findPath("listId").asLong();

        getFavouriteFilmService().addFilmToFavourite(filmId, listId);
        return ok("success");
    }

    public Result isAlreadyFavouriteFilm(long filmId) {
        boolean isFavourite = getFavouriteFilmService().isFavouriteFilm(filmId);

        ObjectNode result = Json.newObject();
        result.put("isFavourite", isFavourite);
        return ok(result);
    }

    public Result showListFilms(long listId) throws IOException {
        List<String> filmsDataJsonList = getListFilms(listId);
        String sharingLink = getFavouriteFilmService().shareFavouriteList(listId);

        return ok(listFilms.render(filmsDataJsonList, sharingLink));
    }

    public Result showSharedList(String link) throws IOException {
        FavouriteList list = getFavouriteFilmService().getList(link);
        List<String> filmsDataJsonList = getListFilms(list.id);

        return ok(sharedList.render(filmsDataJsonList));
    }

    public Result showListCreationPage() {
        return ok(listCreation.render());
    }

    public Result saveList() {
        JsonNode json = request().body().asJson();
        String listName = json.findPath("listName").asText();

        getFavouriteFilmService().createFavouriteList(listName);
        return redirect("/index");
    }

    private List<String> getListFilms(long listId) throws IOException {
        List<FavouriteFilm> favFilmsList = getFavouriteFilmService().getListFilms(listId);

        List<String> filmsDataJsonList = new ArrayList<>();
        for(FavouriteFilm film : favFilmsList) {
            filmsDataJsonList.add(getFilmData(film.id));
        }
        return filmsDataJsonList;
    }

    private String getFilmData(long filmId) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet("http://api.themoviedb.org/3/movie/" + filmId + "?api_key=7a4de0fe5da237bdb52d1168dae8cd14");
        HttpResponse response = client.execute(request);

        return EntityUtils.toString(response.getEntity());
    }

    protected FavouriteFilmService getFavouriteFilmService() {
        return (FavouriteFilmService) ctx.getBean("favouriteFilmService");
    }
}
