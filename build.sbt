name := """FavFilmsApp"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  "mysql" % "mysql-connector-java" % "5.1.38",
  "org.mockito" % "mockito-all" % "1.9.5",
  "org.springframework" % "spring-context" % "4.2.4.RELEASE",
  "org.springframework" % "spring-beans" % "4.2.4.RELEASE",
  "org.springframework" % "spring-core" % "4.2.4.RELEASE",
  "javax.inject" % "javax.inject" % "1"
)

playEbeanModels in Compile := Seq("models.entities.*")
inConfig(Test)(PlayEbean.scopedSettings)

playEbeanModels in Test := Seq("models.entities.*")

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator