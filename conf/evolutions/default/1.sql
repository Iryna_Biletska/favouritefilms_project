# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table favourite_film (
  id                        bigint auto_increment not null,
  list_id                   bigint,
  constraint pk_favourite_film primary key (id))
;

create table favourite_list (
  id                        bigint auto_increment not null,
  list_name                 varchar(255),
  sharing_link              varchar(255),
  constraint pk_favourite_list primary key (id))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table favourite_film;

drop table favourite_list;

SET FOREIGN_KEY_CHECKS=1;

