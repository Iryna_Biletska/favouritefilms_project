package services;

import models.FavouriteListsDAO;
import models.FavouriteListsDAOImpl;
import models.FilmsDAO;
import models.FilmsDAOImpl;
import models.entities.FavouriteFilm;
import models.entities.FavouriteList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FavouriteFilmServiceImplTest {

    private static final long FILM_ID = 975;
    private static final long LIST_ID = 784;
    private static final String LIST_NAME = "my favourite films";
    private static final String PART_OF_SHARING_LINK = "j3jh2ko2u2l4bv623yv3xs3";

    @Mock
    private FavouriteListsDAOImpl favouriteListsDAO;

    @Mock
    private FilmsDAOImpl filmsDAO;

    @InjectMocks
    private FavouriteFilmService favFilmService = new FavouriteFilmServiceImpl();

    @Test
    public void shouldAddFilmToFavouriteList() {
        //when
        favFilmService.addFilmToFavourite(FILM_ID, LIST_ID);
        //then
        verify(filmsDAO).addFilmToList(FILM_ID, LIST_ID);
    }

    @Test
    public void shouldReturnTrueIfFilmIsAlreadyInFavourite() {
        //given
        when(filmsDAO.isFavourite(FILM_ID)).thenReturn(true);
        //when
        boolean result = favFilmService.isFavouriteFilm(FILM_ID);
        //then
        assertTrue(result);
    }

    @Test
    public void shouldReturnFalseIfFilmIsntInFavouriteLists() {
        //given
        when(filmsDAO.isFavourite(FILM_ID)).thenReturn(false);
        //when
        boolean result = favFilmService.isFavouriteFilm(FILM_ID);
        //then
        assertFalse(result);
    }

    @Test
    public void shouldCreateFavouriteList() {
        //when
        favFilmService.createFavouriteList(LIST_NAME);
        //then
        verify(favouriteListsDAO).createList(LIST_NAME);
    }

    @Test
    public void shouldShareFavouriteList() {
        //given
        when(favouriteListsDAO.getSharingLink(LIST_ID)).thenReturn(PART_OF_SHARING_LINK);
        //when
        String result = favFilmService.shareFavouriteList(LIST_ID);
        //then
        assertEquals(PART_OF_SHARING_LINK, result);
    }

    @Test
    public void shouldGetAllLists() {
        //given
        FavouriteList favList = new FavouriteList();
        favList.id = LIST_ID;
        favList.listName = LIST_NAME;

        List<FavouriteList> expectedFavouriteList = Arrays.asList(favList);

        when(favouriteListsDAO.getAllLists()).thenReturn(expectedFavouriteList);
        //when
        List<FavouriteList> actualFavouriteList = favFilmService.getAllLists();
        //then
        assertEquals(expectedFavouriteList, actualFavouriteList);
    }

    @Test
    public void shouldGetListFilms() {
        //given
        FavouriteFilm film = new FavouriteFilm();
        film.id = FILM_ID;
        film.listId = LIST_ID;

        List<FavouriteFilm> expectedFavFilmsList = Arrays.asList(film);

        when(favouriteListsDAO.getListFilms(LIST_ID)).thenReturn(expectedFavFilmsList);
        //when
        List<FavouriteFilm> actualFavFilmList = favFilmService.getListFilms(LIST_ID);
        //then
        assertEquals(expectedFavFilmsList, actualFavFilmList);
    }

    @Test
    public void shouldGetListByPartOfSharingLink() {
        //given
        FavouriteList expectedFavList = new FavouriteList();
        expectedFavList.id = LIST_ID;
        when(favouriteListsDAO.getListBySharingLink(PART_OF_SHARING_LINK)).thenReturn(expectedFavList);
        //when
        FavouriteList favList = favFilmService.getList(PART_OF_SHARING_LINK);
        //then
        assertEquals(expectedFavList, favList);
    }
}
