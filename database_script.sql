create database `fav_films_db`;

create table `favourite_list` (
	`id` int not null auto_increment primary key,
    `list_name` varchar(50),
    `sharing_link` varchar(50)
);

create table `favourite_film` (
	`id` varchar(50) not null primary key,
    `list_id` int,
    foreign key (`list_id`) references `favourite_list`(`id`)
);

insert into `favourite_list` (`list_name`, `sharing_link`) values ("default", "hd6sj2k24d8jrr9");
insert into `favourite_film` (`id`, `list_id`) values (772, 1);