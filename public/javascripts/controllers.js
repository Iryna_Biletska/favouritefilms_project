var favFilmsApp = angular.module('FavFilmsApp', []);

favFilmsApp.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);

favFilmsApp.controller('FilmsController', ['$scope', '$http', function($scope, $http) {
    $scope.searchFilm = function(film) {
        $http.get('http://api.themoviedb.org/3/search/movie?query=' + film.title + '&api_key=7a4de0fe5da237bdb52d1168dae8cd14').success(function(data) {
            $scope.films = data.results;
        });
    };

    $scope.isFavourite = function(filmId) {
        $http.get('/favourite/' + filmId).success(function(data) {
            if(data.isFavourite) {
                $("#alreadyFavFilm_" + filmId).show();
            } else {
                $("#filmToFav_" + filmId).show();
            }
        });
    };

    $scope.addFilmToFavourite = function(filmId) {
        var listId = $("#filmToFav_" + filmId + " > select").val();
        var filmData = {
            filmId : filmId,
            listId : listId
        };
        $http.post('/addFilmToFavourite', filmData).success(function(data) {
            $("#alreadyFavFilm_" + filmId).show();
            $("#filmToFav_" + filmId).hide();
        });
    };

    $scope.getFilmData = function(films) {
        $http.get('http://api.themoviedb.org/3/movie/' + filmId + '?api_key=7a4de0fe5da237bdb52d1168dae8cd14').success(function(data) {
            var title = data.title;
            $("#film_" + filmId + " > h3").text(title);
        });
    };

    $scope.saveList = function(list) {
        if (isCorrectListName(list)){
            $("#saving-error").text("");
            var listData = {listName: list.name};
            $http.post('/saveList', listData);
        } else {
            $("#saving-error").text("Enter a list name.");
        }
    };

    function isCorrectListName(list) {
        return typeof list != "undefined" && list.name.trim().length > 0;
    }
}]);